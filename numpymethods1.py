# np.insert()
'''
np.insert()
numpy.insert(arr, indices, values, axis=None)
Insert values along the given axis before the given indices.
indices defines the index or indices before which the given values are inserted.
values to insert into arr. If the type of values is different from that of arr, values is converted to the type of arr. values should be shaped so that arr[...,indices,...] = values is legal.
Output is a copy of arr with values appended to the specified axis.
'''
import numpy as np
a=np.array([ [1,2,3],
             [4,5,6],
             [7,8,9]])
insert_axis=np.insert(a,3,[7,8,6],axis=0) # here axis 0 means in vertical direction index is 3 from top to bottom it is 3 then place that row in 3rd index 
print("insert at position-3  along axis-0:\n", insert_axis)
insert_axis_1=np.insert(a,3,[1,4,3],axis=1)
print("insert at position-3  along axis-0:\n", insert_axis_1) #here axis 1 means in horizontal direction 
a = np.array([[1, -1], 
              [2, -2], 
              [3, -3]])
zeros = np.zeros((3,2))
insert_axis_2=np.insert(a, [1], zeros, axis=1) # here zeros with 3 rows and 2 columns are created 
print(insert_axis_2)
a = np.array([[1, -1], 
              [2, -2], 
              [3, -3]])

insert_axis_3=np.insert(a, 1, 4, axis=None) # when no axis was given it becomes as a flat array so first index will be replaced by 4 
print(insert_axis_3)

# np.append()
'''
numpy.append(arr, values, axis=None)
Append values to the end of an array.
Output is a copy of arr with values appended to axis.
If axis is given, both arr and values should have the same shape
'''
array_2d = np.array([[1, 2, 3], 
              [4, 5, 6]])

values = np.array([[-1, -2, -3], 
              [-4, -5, -6]])

print(np.append(array_2d, values, axis=0))
print(np.append(array_2d, values, axis=1))

#If axis is not given, both arr and values are flattened before use.
