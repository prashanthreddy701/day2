# Advanced indexing-- group of elements which are not in order (arbitrary elements)
#When the elements are not ordered then we will go for Advanced indexing

'''
1-D array ==> a[begin:end:step]
2-D array ==> a[begin:end:step,begin:end:step]
3-D array ==> a[begin:end:step,begin:end:step,begin:end:step]
'''
import numpy as np
a = np.arange(10,101,10)
indices=[2,3,8,1]
print(a[indices])
# a[[row_indices],[column_indices]]
a=np.arange(1,17)
result=a.reshape(4,4)
print(result[[0,1,2],[0,1,2]])
print("***")
print(result[1][1])

#a[[indices of 2d array],[row indices],[column indices]]

a = np.arange(1,25).reshape(2,3,4)
print(a)
#array[2-D array index][row_index_in that 2-D array][columnindex in that 2-d array]
print("#")
print(a[0][2][1])
# have to select 7 and 18
# 7 belongs to first layer so 1 18 belongs to second layerso 1 [0,1]
# in rows 7 belongs to 1 st row and 18 belongs to 1st row so [1,1]
# in columns 7 belongs to 2nd column and 18 belongs to first column so [2,1]
print("---")
print(a[[0,1],[1,1],[2,1]])

'''
Basic Indexing
1-D array :: a[i]
2-D array :: a[i][j] or a[i,j]
3-D array :: a[i][j][k] or a[i,j,k]
Slicing
1-D array :: a[begin:end:step]
2-D array :: a[begin:end:step,begin:end:step]
3-D array :: a[begin:end:step,begin:end:step,begin:end:step]
Advanced Indexing
1-D array :: a[x] --> x contains required indices of type ndarray or list
2-D array :: a[[row indices],[column indices]]
3-D array :: a[[indices of 2D array],[row indices],[column indices]]
'''
